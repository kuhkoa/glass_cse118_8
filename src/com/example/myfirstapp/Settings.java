package com.example.myfirstapp;

public class Settings {
	public static final String SHARED_PREF_KEY = "ChromaPrefs";
	public static final String COLOR_PREF_STRING = "COLOR_PREF";
	public static final int RED = 0;
	public static final int GREEN = 1;
	public static final int BOTH = 2;
	public static final int CONT = 2; // change to 3 when actually working
}
