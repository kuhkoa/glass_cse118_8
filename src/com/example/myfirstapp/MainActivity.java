package com.example.myfirstapp;

import java.io.File;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

/*
 * CLASS MainActivity
 */
public class MainActivity extends Activity {

	private GestureDetector gestureDetector;
	private ImageView imageView;
	private TextView textView;

	private SharedPreferences sharedPreference;
	private SharedPreferences.Editor sharedPreferenceEditor;
	private int colorPreference;

	@Override
	/*
	 * Direct tap events to gestureDetector
	 */
	public boolean onGenericMotionEvent(MotionEvent event)
	{
		gestureDetector.onTouchEvent(event);
		return true;
	}

	@Override
	/*
	 * Standard function for every app
	 * 
	 * Call this when the app starts
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		gestureDetector = new GestureDetector(this, new MyGestureListener());
		imageView = (ImageView) findViewById(R.id.imageView1);
		textView = (TextView) findViewById(R.id.textView1);
		
		imageView.setScaleType(ScaleType.FIT_XY);
		imageView.setVisibility(View.INVISIBLE);
		
		sharedPreference = getSharedPreferences(Settings.SHARED_PREF_KEY, MODE_PRIVATE);
		colorPreference = sharedPreference.getInt(Settings.COLOR_PREF_STRING, -1);
		if (colorPreference == -1)
		{
			// launch options menu
			Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
			this.startActivity(myIntent);		
		}
	}
	
	protected void onDestroy()
	{
		super.onDestroy();
		sharedPreferenceEditor = sharedPreference.edit();
		sharedPreferenceEditor.putInt(Settings.COLOR_PREF_STRING, -1);
		sharedPreferenceEditor.commit();
	}

	protected void onResume()
	{
		super.onResume();
		sharedPreference = getSharedPreferences(Settings.SHARED_PREF_KEY, MODE_PRIVATE);
		colorPreference = sharedPreference.getInt(Settings.COLOR_PREF_STRING, -1);
		Log.d("MainActivity", "color selected = " + colorPreference);
	}

	@Override
	/*
	 * Standard function for every app
	 */
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/*
	 * Go open the external camera app and take a picture
	 */
	void dispatchTakePictureIntent(int actionCode) {
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		startActivityForResult(takePictureIntent, actionCode);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		imageView.setVisibility(View.INVISIBLE);
		textView.setVisibility(View.VISIBLE);
		textView.setText("Processing image...");
	}

	@Override
	/*
	 * Call this method after the external camera app is done taking a picture
	 */
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
Log.d("KOA", "onActivityResult called");
		Bundle extras;
		try{
			extras = data.getExtras();
		}
		catch(Exception e)
		{
			textView.setText("Tap to take a shot");
			Log.d("myapp", "Photo was not taken successfully");
			return;
		}

		Bitmap myBitmap;

		String filePath = (String) extras.get("picture_file_path");
		for (String s : extras.keySet())
			Log.d("myapp", s);

		/*
		 *  This does not work for Glass, but works for Android phone
		 */
		//myBitmap = (Bitmap) extras.get("data");

		/*
		 * This does not work for Android phone, but works for Glass
		 */
		File imageFile = new File(filePath);
		while (!imageFile.exists())
		{
			Log.d("myapp", "FATAL ERROR! Cannot find " + filePath);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		myBitmap = decodeSampledBitmapFromResource(filePath, 480, 800);

		/*
		 * Modify the bitmap
		 */
		int[] pixels = new int[myBitmap.getHeight()*myBitmap.getWidth()];
		myBitmap.getPixels(pixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());
		for (int i=0; i<pixels.length; i++)
		{
			int red = Color.red(pixels[i]);
			int green = Color.green(pixels[i]);
			int blue = Color.blue(pixels[i]);

			if (colorPreference == Settings.BOTH || colorPreference == Settings.RED)
			{
				if (blue < 80 && green < 65 && red > green + 50)
				{
					pixels[i] = Color.RED;
				}

				if (blue >= 80 && red > 130 && red > green + 50)
				{
					pixels[i] = Color.RED;
				}
			}
			
			if (colorPreference == Settings.BOTH || colorPreference == Settings.GREEN)
			{
				int redBlueDiff = Math.abs(red - blue);

				if ((green > 70 && green > red + blue) || (green >= 254 && red < 230 && blue < 230 && redBlueDiff < 20))
				{
					pixels[i] = Color.GREEN;
				}
				
				if ((redBlueDiff < 30 && (green > red + 30 || green > blue + 30)) && green > 70)//blue < 150 && green > 70 && red < 150)
				{
					pixels[i] = Color.GREEN;
				}
				
				if (redBlueDiff >= 30 && green > red + 30 && green > blue + 30)
				{
					pixels[i] = Color.GREEN;
				}
			}
		}
		myBitmap.setPixels(pixels, 0, myBitmap.getWidth(), 0, 0, myBitmap.getWidth(), myBitmap.getHeight());

		/*
		 * Display the image
		 */
		 imageView.setImageBitmap(myBitmap);
		 textView.setVisibility(View.INVISIBLE);
		 imageView.setVisibility(View.VISIBLE);
		 Log.d("myapp", "Done displaying image");
	}

	/*
	 * GestureListener for glass to respond to tap
	 */
	class MyGestureListener extends android.view.GestureDetector.SimpleOnGestureListener
	{
		@Override
		public boolean onSingleTapUp(MotionEvent e) {
			Log.d("myapp", "Tapped!!!");
			dispatchTakePictureIntent(0);
			return true;
		}
	}

	/*
	 * GestureListener does not work for the phone, so we need a button.
	 * 
	 * This is the method that will be invoked when the button is clicked.
	 */
	Button.OnClickListener mTakePicOnClickListener = 
			new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.d("myapp", "Button clicked!");
			dispatchTakePictureIntent(0);
		}
	};

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}


	public static Bitmap decodeSampledBitmapFromResource(String path,
			int reqWidth, int reqHeight) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		options.inMutable = true;
		BitmapFactory.decodeFile(path, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeFile(path, options);
	}
}
